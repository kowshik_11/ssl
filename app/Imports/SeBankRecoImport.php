<?php

namespace App\Imports;

use App\SeBankReco;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;

class SeBankRecoImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new SeBankReco([
            'merchant_id' => $row[1],
            'masked_pan' => $row[2],
            'transaction_datetime'   => $row[3],
            'authorization_code' =>  (int) $row[4],
            'retrieval_reference_number' => $row[5],
            'transaction_amount' => $row[7],
            'transaction_type' => $row[9],
            'refund_amount' => $row[12],
            'net_settlement_amount' => $row[13],
            'commission_amount' => $row[14],
            'file_name' => $this->fileName,
            'settlement_date' => $row[11],
        ]);
    }
}
