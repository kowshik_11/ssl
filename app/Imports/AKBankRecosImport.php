<?php

namespace App\Imports;

use App\AKBankReco;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMappedCells;
class AKBankRecosImport implements WithMappedCells,ToModel
{
    private $fileName="";
    // public function __construct($fileName){
    //     $this->fileName=$fileName;
    //     // dd($fileName);

    // }
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    public function mapping(): array
    {
        return [
            '1' => 'B1',
            '2' => 'C1',
            '3' => 'D1',
            '4' => 'E1',
            '5' => 'F1',
            '6' => 'J1',
            '7' => 'N1',
            '8' => 'O1'
        ];
    }
    public function model(array $row)
    {
        $processing_date = ($row['1'] - 25569) * 86400;
        $processing_date= gmdate("Y-m-d", $processing_date);
        $transaction_date = ($row['2'] - 25569) * 86400;
        $transaction_date= gmdate("Y-m-d", $transaction_date);
        return new AKBankReco([
            'processing_date'         => $processing_date,
            'transaction_date'        => $transaction_date,
            'merchant_id'           => $row['1'],
            'masked_pan'        => $row['2'],
            'transaction_ad'        => (double)$row['5'],
            'transaction_amount'      => (double)$row['6'],
            'discount_amount'         => (double)$row['7'],
            'marchent_settled_amount' => (double)$row['8'],
            'transaction_defination'  => $row['9'],
            'filename'                => $this->fileName
        ]);
    }
}