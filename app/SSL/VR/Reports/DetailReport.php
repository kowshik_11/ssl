<?php namespace SSL\VR\Reports;

class DetailReport
{
    private $clients = array();
    private $startDate;
    private $endDate;
    private $request;

    /**
     * @param array $clients
     */
    public function setClients($clients)
    {
        $this->clients = $clients;
        return $this;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    public function setRequest($request) {
        $this->request = $request;
    }

    private function validate()
    {
        $this->formatParam('clients');
        $this->formatParam('departments');
        $this->formatParam('kams');
        $this->formatParam('operators');
    }

    private function formatParam($name)
    {
        if($this->request->has($name))
            $this->client = $this->client->{$name}($this->request{$name});
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function generateReport()
    {
        $this->validate();
        return $this->clients->get();
    }
}