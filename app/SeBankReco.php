<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeBankReco extends Model
{
    protected $table="se_bank_reco";
    public $timestamps = false;
    protected $fillable = [
        'merchant_id'   ,
        'masked_pan'        ,
        'transaction_datetime'       ,
        'authorization_code' ,
        'retrieval_reference_number' ,
        'transaction_amount',
        'transaction_type' ,
        'refund_amount' ,
        'net_settlement_amount' ,
        'commission_amount' ,
        'file_name',
        'settlement_date'
    ];
}
