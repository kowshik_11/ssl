<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AKBankReco extends Model
{
    protected $table="ak_bank_reco";
    protected $guarded=[];
    public $timestamps = false;
    protected $primaryKey = null;
    public $incrementing = false;
}
