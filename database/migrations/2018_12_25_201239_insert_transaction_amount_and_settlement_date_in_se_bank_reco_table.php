<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertTransactionAmountAndSettlementDateInSeBankRecoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('se_bank_reco', function($table) {
            $table->double('transaction_amount');
            $table->date('settlement_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('se_bank_reco', function($table) {
            $table->dropColumn('transaction_amount');
            $table->dropColumn('settlement_date');
        });
    }
}
