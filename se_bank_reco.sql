-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2018 at 07:30 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sslwirelessbi`
--

-- --------------------------------------------------------

--
-- Table structure for table `se_bank_reco`
--

CREATE TABLE `se_bank_reco` (
  `id` int(11) NOT NULL,
  `merchant_id` varchar(200) DEFAULT NULL,
  `masked_pan` varchar(225) DEFAULT NULL,
  `transaction_date_time` datetime DEFAULT NULL,
  `authorization_code` bigint(20) DEFAULT NULL,
  `retrieval_reference_number` double DEFAULT NULL,
  `transaction_type` varchar(225) DEFAULT NULL,
  `refund_amount` double DEFAULT NULL,
  `net_settlement_amount` double DEFAULT NULL,
  `commission_amount` double DEFAULT NULL,
  `file_name` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `se_bank_reco`
--

INSERT INTO `se_bank_reco` (`id`, `merchant_id`, `masked_pan`, `transaction_date_time`, `authorization_code`, `retrieval_reference_number`, `transaction_type`, `refund_amount`, `net_settlement_amount`, `commission_amount`, `file_name`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ak vr receable.xlsx\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `se_bank_reco`
--
ALTER TABLE `se_bank_reco`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `se_bank_reco`
--
ALTER TABLE `se_bank_reco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
